import { useState, useRef, useEffect } from "react";
import ButtonArray from "./ButtonArray";
import Cell from "./Cell";

function App() {
  // const [gridsize, setGridsize] = useState(20);
  const initialCellsAliveLimit = 0.2;
  const gridsize = 10;
  const [grid, setGrid] = useState(
    new Array(gridsize).fill(0).map(() =>
      new Array(gridsize).fill({
        isAlive: false,
      })
    )
  );
  const [speed, setSpeed] = useState(0);

  const speedRef = useRef(speed);

  useEffect(() => {
    function randomizeStart() {
      setGrid(
        grid.map((row, _) =>
          row.map((__, ___) => {
            return {
              isAlive: Math.random() < initialCellsAliveLimit ? true : false,
            };
          })
        )
      );
    }

    randomizeStart();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    var timeouts = [];
    const clearTimer = () => {
      timeouts.forEach((t) => clearTimeout(t));
      timeouts = [];
    };

    const repeatTimeout = () => {
      timeouts.push(
        setTimeout(() => {
          if (speedRef.current > 0) {
            console.log("running", speedRef.current);
            progressGeneration();
            repeatTimeout();
          }
        }, 5000 / speedRef.current)
      );
    };

    speedRef.current = speed;

    if (speedRef.current > 0) {
      repeatTimeout();
    } else {
      clearTimer();
    }

    return () => clearTimer();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [speed, grid]);

  const toggleCell = (r, c) => {
    setGrid(
      grid.map((row, rowIndex) =>
        row.map((cell, colIndex) => {
          let currentState = cell.isAlive;
          return {
            isAlive:
              r === rowIndex && c === colIndex ? !currentState : currentState,
          };
        })
      )
    );
  };

  const progressGeneration = () => {
    setGrid(
      grid.map((row, rowIndex) =>
        row.map((col, colIndex) => {
          return cellCheck(rowIndex, colIndex);
        })
      )
    );
  };

  const clearGrid = () => {
    setGrid(
      new Array(gridsize)
        .fill(0)
        .map(() => new Array(gridsize).fill({ isAlive: false }))
    );
  };

  const preventDragHandler = (e) => {
    e.preventDefault();
  };

  const cellCheck = (row, col) => {
    let neighbors = [
      row > 0 && col > 0 ? grid[row - 1][col - 1] : 0,
      row > 0 ? grid[row - 1][col] : 0,
      row > 0 && col < gridsize - 1 ? grid[row - 1][col + 1] : 0,
      col > 0 ? grid[row][col - 1] : 0,
      col < gridsize - 1 ? grid[row][col + 1] : 0,
      row < gridsize - 1 && col > 0 ? grid[row + 1][col - 1] : 0,
      row < gridsize - 1 ? grid[row + 1][col] : 0,
      row < gridsize - 1 && col < gridsize - 1 ? grid[row + 1][col + 1] : 0,
    ];

    let count = neighbors.reduce(
      (total, current) => total + (current.isAlive ? 1 : 0),
      0
    );

    let currentState = grid[row][col].isAlive;
    // If cell is alive and count > 3 or < 1 -> dead
    // If cell is alive and count is 2,3 -> alive
    // If cell is dead and count is 3 -> alive

    return {
      isAlive: count === 3 || (currentState && count === 2) ? true : false,
    };
  };

  return (
    <div className="container">
      <h1 className="title">Conway's Game of Life</h1>
      <div className="gridbox" onDragStart={preventDragHandler}>
        {grid.map((row, rowIndex) => {
          return (
            <div className="row" key={rowIndex}>
              {row.map((col, colIndex) => (
                <Cell
                  key={colIndex}
                  row={rowIndex}
                  col={colIndex}
                  cell={grid[rowIndex][colIndex]}
                  toggleCell={toggleCell}
                />
              ))}
            </div>
          );
        })}
      </div>
      <ButtonArray
        progressGrid={progressGeneration}
        clearGrid={clearGrid}
        speed={speed}
        setSpeed={setSpeed}
      />
    </div>
  );
}

export default App;
