import { useState } from "react";

const ButtonArray = ({progressGrid, clearGrid, speed, setSpeed}) => {

    const [left, setLeft] = useState('0%');
    
    const progressClick = () => {
        progressGrid();
    }

    const dragSlider = (e) => {
        let speedVal = e.target.value;
        setSpeed(speedVal);
        let speedMax = Number(e.target.max);
        let scrollWidth = e.target.scrollWidth;
        let width = e.target.offsetParent.clientHeight;
        let left = `${((speedVal / speedMax) * (scrollWidth-width))}px`
        setLeft(left)
    }
   
    return ( 
        <>
            <div className="buttonBar">
                <button onClick={progressClick} >
                    <span> { '>' } </span>
                </button>
                <div className="rangeWrap">
                    <input 
                        type="range" 
                        id="rangeSlider"
                        min={0}
                        max={8}
                        step={1}
                        onChange={dragSlider}
                        value={speed}
                    />
                    <div 
                        className="bubble"
                        style={{ left }}
                        
                    >{`x${speed}`}</div>
                </div>
                <button onClick={() => clearGrid()} >
                    <span> {'[]'} </span>
                </button>
            </div>
        </>
     );
}
 
export default ButtonArray;