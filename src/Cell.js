const Cell = ({row, col, cell, toggleCell}) => {

    const {isAlive} = cell;

    const handleOnClick = (e) => {
        if (e.buttons === 1 || e.type === 'mousedown') {
            toggleCell(row, col);
        }
    }
    
    return ( 
        <div 
            className={`cell ${isAlive && 'alive'}`} 
            onMouseDown={handleOnClick}
            onMouseOver={handleOnClick} 
        >             
        </div>
     );
}
 
export default Cell;