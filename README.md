# GAME OF LIFE - REACTJS

This is an implementation of [Conway's game of life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life) using [ReactJS](https://reactjs.org/).

Check out the app running [here](https://reactjs-game-of-life.netlify.app/).
